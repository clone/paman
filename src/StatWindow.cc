/* $Id$ */

/***
  This file is part of paman.
 
  paman is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 2 of the License,
  or (at your option) any later version.
 
  paman is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with paman; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "paman.hh"
#include "StatWindow.hh"

#define GLADE_NAME "statWindow"

StatWindow::StatWindow(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade) :
    Gtk::Window(cobject),
    totalLabel(NULL),
    totalSizeLabel(NULL),
    allocatedLabel(NULL),
    allocatedSizeLabel(NULL),
    sampleCacheLabel(NULL),
    closeButton(NULL),
    refreshButton(NULL),
    operation(NULL) {

    refGlade->get_widget("totalLabel", totalLabel);
    refGlade->get_widget("totalSizeLabel", totalSizeLabel);
    refGlade->get_widget("allocatedLabel", allocatedLabel);
    refGlade->get_widget("allocatedSizeLabel", allocatedSizeLabel);
    refGlade->get_widget("sampleCacheLabel", sampleCacheLabel);
    refGlade->get_widget("closeButton", closeButton);
    refGlade->get_widget("refreshButton", refreshButton);

    closeButton->signal_clicked().connect(sigc::mem_fun(*this, &StatWindow::onCloseButton));
    refreshButton->signal_clicked().connect(sigc::mem_fun(*this, &StatWindow::onRefreshButton));

    onRefreshButton();
}

StatWindow::~StatWindow() {

    if (operation) {
        pa_operation_cancel(operation);
        pa_operation_unref(operation);
    }
}

StatWindow* StatWindow::create() {
    StatWindow *w = NULL;
    Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create(GLADE_FILE, GLADE_NAME);
    refXml->get_widget_derived(GLADE_NAME, w);
    return w;
}

void StatWindow::onCloseButton() {
    hide();
}

static void stat_cb(struct pa_context *, const struct pa_stat_info *i, void *userdata) {
    char t[20];
    StatWindow *s = (struct StatWindow*) userdata;

    snprintf(t, sizeof(t), "%u", i->memblock_total);
    s->totalLabel->set_text(t);
    pa_bytes_snprint(t, sizeof(t), i->memblock_total_size);
    s->totalSizeLabel->set_text(t);
    snprintf(t, sizeof(t), "%u", i->memblock_allocated);
    s->allocatedLabel->set_text(t);
    pa_bytes_snprint(t, sizeof(t), i->memblock_allocated_size);
    s->allocatedSizeLabel->set_text(t);
    pa_bytes_snprint(t, sizeof(t), i->scache_size);
    s->sampleCacheLabel->set_text(t);

    pa_operation_unref(s->operation);
    s->operation = NULL;
}

void StatWindow::onRefreshButton() {
    if (operation)
        return;

    g_assert(context);
    operation = pa_context_stat(context, stat_cb, this);
}

void StatWindow::present() {
    Gtk::Window::present();

    onRefreshButton();
}

bool StatWindow::on_delete_event(GdkEventAny*) {
    hide();
    return false;
}
