#ifndef foosinkwindowhhfoo
#define foosinkwindowhhfoo

/* $Id$ */

/***
  This file is part of paman.
 
  paman is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 2 of the License,
  or (at your option) any later version.
 
  paman is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with paman; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
***/

#include <gtkmm.h>
#include <libglademm.h>

class SinkWindow;

#include "ServerInfoManager.hh"

class SinkWindow : public Gtk::Window {
public:
    SinkWindow(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade);
    static SinkWindow* create();

    Gtk::Label *nameLabel,
        *descriptionLabel,
        *indexLabel,
        *sampleTypeLabel,
        *channelMapLabel,
        *latencyLabel,
        *ownerModuleLabel,
        *monitorSourceLabel,
        *volumeLabel;

    Gtk::Button *closeButton,
        *toMonitorSourceButton,
        *toOwnerModuleButton,
        *volumeResetButton,
        *volumeMuteButton,
        *volumeMeterButton;

    Gtk::HScale *volumeScale;

    uint32_t index, owner_module, monitor_source;
    Glib::ustring monitor_source_name;
    bool scaleEnabled;
    
    void updateInfo(const SinkInfo &i);

    virtual void onCloseButton();
    virtual void onToMonitorSourceButton();
    virtual void onToOwnerModuleButton();
    virtual void onVolumeScaleValueChanged();
    virtual void onVolumeResetButton();
    virtual void onVolumeMuteButton();
    virtual void onVolumeMeterButton();

    virtual bool on_delete_event(GdkEventAny* e);
};

#endif
