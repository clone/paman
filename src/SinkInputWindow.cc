/* $Id$ */

/***
  This file is part of paman.
 
  paman is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 2 of the License,
  or (at your option) any later version.
 
  paman is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with paman; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include "paman.hh"
#include "SinkInputWindow.hh"

#define GLADE_NAME "sinkInputWindow"

SinkInputWindow::SinkInputWindow(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade) :
    Gtk::Window(cobject),
    nameLabel(NULL),
    indexLabel(NULL),
    sampleTypeLabel(NULL),
    channelMapLabel(NULL),
    latencyLabel(NULL),
    sinkLabel(NULL),
    clientLabel(NULL),
    ownerModuleLabel(NULL),
    volumeLabel(NULL),
    resampleMethodLabel(NULL),
    closeButton(NULL),
    toOwnerModuleButton(NULL),
    toClientButton(NULL),
    toSinkButton(NULL),
    volumeResetButton(NULL),
    volumeMuteButton(NULL),
    killButton(NULL),
    volumeScale(NULL),
    scaleEnabled(true) {

    refGlade->get_widget("nameLabel", nameLabel);
    refGlade->get_widget("indexLabel", indexLabel);
    refGlade->get_widget("sampleTypeLabel", sampleTypeLabel);
    refGlade->get_widget("channelMapLabel", channelMapLabel);
    refGlade->get_widget("latencyLabel", latencyLabel);
    refGlade->get_widget("sinkLabel", sinkLabel);
    refGlade->get_widget("clientLabel", clientLabel);
    refGlade->get_widget("ownerModuleLabel", ownerModuleLabel);
    refGlade->get_widget("closeButton", closeButton);
    refGlade->get_widget("toOwnerModuleButton", toOwnerModuleButton);
    refGlade->get_widget("toClientButton", toClientButton);
    refGlade->get_widget("toSinkButton", toSinkButton);
    refGlade->get_widget("volumeLabel", volumeLabel);
    refGlade->get_widget("volumeScale", volumeScale);
    refGlade->get_widget("volumeResetButton", volumeResetButton);
    refGlade->get_widget("volumeMuteButton", volumeMuteButton);
    refGlade->get_widget("killButton", killButton);
    refGlade->get_widget("resampleMethodLabel", resampleMethodLabel);

    closeButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onCloseButton));
    toOwnerModuleButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onToOwnerModuleButton));
    toClientButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onToClientButton));
    toSinkButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onToSinkButton));
    volumeScale->signal_value_changed().connect(sigc::mem_fun(*this, &SinkInputWindow::onVolumeScaleValueChanged));
    volumeResetButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onVolumeResetButton));
    volumeMuteButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onVolumeMuteButton));
    killButton->signal_clicked().connect(sigc::mem_fun(*this, &SinkInputWindow::onKillButton));
}

SinkInputWindow* SinkInputWindow::create() {
    SinkInputWindow *w = NULL;
    Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create(GLADE_FILE, GLADE_NAME);
    refXml->get_widget_derived(GLADE_NAME, w);
    return w;
}

void SinkInputWindow::updateInfo(const SinkInputInfo &i) {
    char t[80], ss[PA_SAMPLE_SPEC_SNPRINT_MAX], cm[PA_CHANNEL_MAP_SNPRINT_MAX];
    double percent, db;

    nameLabel->set_text(i.name);
    snprintf(t, sizeof(t), "#%u", i.index);
    indexLabel->set_text(t);

    sampleTypeLabel->set_text(pa_sample_spec_snprint(ss, sizeof(ss), &i.sample_spec));
    channelMapLabel->set_text(pa_channel_map_snprint(cm, sizeof(cm), &i.channel_map));

    if (i.owner_module == PA_INVALID_INDEX)
        ownerModuleLabel->set_markup("<i>n/a</i>");
    else {
        snprintf(t, sizeof(t), "#%u", i.owner_module);
        ownerModuleLabel->set_text(t);
    }

    snprintf(t, sizeof(t), "%0.0f &#956;s (= buffer: %0.0f &#956;s + sink: %0.0f &#956;s)", (double) i.buffer_usec+i.sink_usec, (double) i.buffer_usec, (double) i.sink_usec);
    latencyLabel->set_markup(t);

    SinkInfo *sink = serverInfoManager->getSinkInfo(i.sink);
    sinkLabel->set_text(sink->name);

    if (i.client == PA_INVALID_INDEX)
        clientLabel->set_markup("<i>n/a</i>");
    else {
        ClientInfo *client = serverInfoManager->getClientInfo(i.client);
        clientLabel->set_text(client->name);
    }

    percent = pa_sw_volume_to_linear(pa_cvolume_avg(&i.volume)) * 100;

    /* FIXME: Hardware volume doesn't translate well to dB. */
    db = pa_sw_volume_to_dB(pa_cvolume_avg(&i.volume));
    scaleEnabled = false;
    volumeScale->set_value(percent);
    scaleEnabled = true;
    if (db != PA_DECIBEL_MININFTY)
        snprintf(t, sizeof(t), "%0.0f%% (%0.2fdB)", percent, db);
    else
        snprintf(t, sizeof(t), "%0.0f%% (-&#8734;dB)", percent);
    volumeLabel->set_markup(t);

    resampleMethodLabel->set_markup(i.resample_method);
    
    set_title("Sink Input: "+i.name);

    this->sink = i.sink;
    client = i.client;
    owner_module = i.owner_module;
    index = i.index;

    toOwnerModuleButton->set_sensitive(owner_module != PA_INVALID_INDEX);
    toClientButton->set_sensitive(client != PA_INVALID_INDEX);
}

void SinkInputWindow::onCloseButton() {
    hide();
}

void SinkInputWindow::onToOwnerModuleButton() {
    if (owner_module != PA_INVALID_INDEX)
        serverInfoManager->showModuleWindow(owner_module);
}

void SinkInputWindow::onToSinkButton() {
    serverInfoManager->showSinkWindow(sink);
}

void SinkInputWindow::onToClientButton() {
    serverInfoManager->showClientWindow(client);
}

void SinkInputWindow::onVolumeScaleValueChanged() {
    if (scaleEnabled)
        serverInfoManager->setSinkInputVolume(index, pa_sw_volume_from_linear(volumeScale->get_value()/100));
}

void SinkInputWindow::onVolumeResetButton() {
    serverInfoManager->setSinkInputVolume(index, PA_VOLUME_NORM);
}

void SinkInputWindow::onVolumeMuteButton() {
    serverInfoManager->setSinkInputVolume(index, PA_VOLUME_MUTED);
}

bool SinkInputWindow::on_delete_event(GdkEventAny* ) {
    hide();
    return false;
}

void SinkInputWindow::onKillButton() {
    serverInfoManager->killSinkInput(index);
}
