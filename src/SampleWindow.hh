#ifndef foosamplewindowhhfoo
#define foosamplewindowhhfoo

/* $Id$ */

/***
  This file is part of paman.
 
  paman is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 2 of the License,
  or (at your option) any later version.
 
  paman is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with paman; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA.
***/

#include <gtkmm.h>
#include <libglademm.h>

class SampleWindow;

#include "ServerInfoManager.hh"

class SampleWindow : public Gtk::Window {
public:
    SampleWindow(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade);
    static SampleWindow* create();

    Gtk::Label *nameLabel,
        *indexLabel,
        *volumeLabel,
        *sampleTypeLabel,
        *durationLabel,
        *sizeLabel,
        *lazyLabel,
        *filenameLabel;

    Gtk::Button *closeButton;
    
    void updateInfo(const SampleInfo &i);

    virtual void onCloseButton();
    virtual bool on_delete_event(GdkEventAny* e);
};

#endif
